# TASK MANAGER
## DEVELOPER INFO
* **NAME:** Babak Artem
* **E-MAIL:** ababak@t1-consulting.ru
* **E-MAIL:** support@t1-consulting.ru
# SOFTWARE
* OpenJDK 8
* Intellij Idea
* Windows 10 Pro x64
## HARDWARE
* **RAM:** 32Gb
* **CPU:** AMD Ryzen 5 3600
* **HDD:** 250Gb
## BUILD APPLICATION
```shell script
mvn clean install
```
## RUN PROGRAM
```shell script
java -jar ./task-manager.jar
```
