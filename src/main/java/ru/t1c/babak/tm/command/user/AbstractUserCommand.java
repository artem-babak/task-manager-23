package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.api.service.IAuthService;
import ru.t1c.babak.tm.api.service.IUserService;
import ru.t1c.babak.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

}
