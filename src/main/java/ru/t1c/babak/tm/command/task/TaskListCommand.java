package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.model.Task;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Show task list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("\tENTER SORT:");
        System.out.println("\t" + Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}
