package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("\tENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("\tENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("\tENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        getServiceLocator().getAuthService().registry(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
