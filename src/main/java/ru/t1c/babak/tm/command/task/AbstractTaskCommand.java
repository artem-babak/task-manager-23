package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.api.service.ITaskService;
import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    /**
     * Render all tasks in default order.
     */
    void renderAllTasks() {
        final List<Task> tasks = getTaskService().findAll();
        renderTasks(tasks);
    }

    void renderTasks(final List<Task> tasks) {
        for (int i = 0; i < tasks.size(); ) {
            final Task task = tasks.get(i++);
            if (task == null) continue;
            System.out.println("\t" + i + ". " + task);
        }
    }

    void showTask(final Task task) {
        showWbs(task);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
