package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.repository.IUserRepository;
import ru.t1c.babak.tm.api.service.IUserService;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.exception.entity.UserNotCreatedException;
import ru.t1c.babak.tm.exception.entity.UserNotFoundException;
import ru.t1c.babak.tm.exception.field.*;
import ru.t1c.babak.tm.model.User;
import ru.t1c.babak.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

//    public UserService(
//            final IUserRepository repository,
//            final ITaskRepository TaskRepository,
//            final IProjectRepository ProjectRepository
//            ) {
//        super(repository);
//    }

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(
            final IUserRepository userRepository,
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        super(userRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }


    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User updateById(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updateByLogin(final String login, final String firstName, final String lastName, final String middleName) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }


    @Override
    public User create(final String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (repository.findByLogin(login) == null) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = repository.create(login, passwordHash);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (repository.findByLogin(login) != null) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        if (repository.findByEmail(email) != null) throw new EmailExistException();
        User user = repository.create(login, passwordHash, email);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (repository.findByLogin(login) != null) throw new LoginExistException();
        if (role == null) throw new RoleEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = repository.create(login, passwordHash, role);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User remove(final User model) {
        if (model == null) return null;
        final User user = super.remove(model);
        if (user == null) return null;
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }


    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return remove(findByLogin(login));
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return remove(findByEmail(email));
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = repository.setPasswordHashById(id, passwordHash);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(false);
    }


}
