package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    List<M> findAll();

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(Collection<M> collection);

    M add(M model);

    List<M> findAll(Comparator<Object> comparator);

    List<M> findAll(Sort sort);

}
