package ru.t1c.babak.tm.api.model;

import ru.t1c.babak.tm.enumerated.Role;

public interface IUser {

    String getId();

    void setId(String id);

    String getLogin();

    void setLogin(String login);

    String getPasswordHash();

    void setPasswordHash(String passwordHash);

    String getEmail();

    void setEmail(String email);

    String getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);

    String getMiddleName();

    void setMiddleName(String middleName);

    Role getRole();

    void setRole(Role role);

}
