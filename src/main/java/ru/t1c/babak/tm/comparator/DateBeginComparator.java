package ru.t1c.babak.tm.comparator;

import ru.t1c.babak.tm.api.model.IHaveDateBegin;

import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHaveDateBegin> {

    INSTANCE;

    @Override
    public int compare(final IHaveDateBegin o1, final IHaveDateBegin o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateBegin() == null || o2.getDateBegin() == null) return 0;
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }

}
